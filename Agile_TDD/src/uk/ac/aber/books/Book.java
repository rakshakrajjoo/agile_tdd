package uk.ac.aber.books;

public class Book {

	private String title;
	private String author;
	private String genre;
	private String decscription;
	private String isbn;
	private Double price;
	
	public Book(String title) {
		this.title = title;
	}
	
	public Book(String title, String author, String genre, String decscription, String isbn, Double price) {
		this.title = title;
		this.author = author;
		this.genre = genre;
		this.decscription = decscription;
		this.isbn = isbn;
		this.price = price;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getDecscription() {
		return decscription;
	}

	public void setDecscription(String decscription) {
		this.decscription = decscription;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
	
}
